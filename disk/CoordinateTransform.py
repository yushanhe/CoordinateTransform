#!/usr/bin/env python3
# -*- coding: utf-8 _*_
import math

__X_PI = 3.14159265358979324 * 3000.0 / 180.0
__PI = 3.1415926535897932384626
__a = 6378245.0
__ee = 0.00669342162296594323


def transformBD09ToGCJ02(lng, lat):
    """ 百度坐标（BD09）转 GCJ02
    :param lng 经度
    :param lat 纬度
    :return GCJ02 坐标：经度，纬度
    """
    x = lng - 0.0065
    y = lat - 0.006
    z = math.sqrt(x * x + y * y) - 0.00002 * math.sin(y * __X_PI)
    theta = math.atan2(y, x) - 0.000003 * math.cos(x * __X_PI)
    gcj_lng = z * math.cos(theta)
    gcj_lat = z * math.sin(theta)
    return gcj_lng, gcj_lat


def transformGCJ02ToBD09(lng, lat):
    """ GCJ02 转百度坐标
    :param lng: 经度
    :param lat: 纬度
    :return: 百度坐标：经度，纬度
    """
    z = math.sqrt(lng * lng + lat * lat) + 0.00002 * math.sin(lat * __X_PI)
    theta = math.atan2(lat, lng) + 0.000003 * math.cos(lng * __X_PI)
    bd_lng = z * math.cos(theta) + 0.0065
    bd_lat = z * math.sin(theta) + 0.006
    return bd_lng, bd_lat


def transformGCJ02ToWGS84(lng, lat):
    """
    GCJ02 转 WGS84
    :param lng:
    :param lat:
    :return:
    """
    if outOfChina(lng, lat):
        return [lng, lat]
    dLat = __transformLat(lng - 105.0, lat - 35.0)
    dLng = __transformLng(lng - 105.0, lat - 35.0)
    radLat = lat / 180.0 * __PI
    magic = math.sin(radLat)
    magic = 1 - __ee * magic * magic
    sqrtMagic = math.sqrt(magic)
    dLat = (dLat * 180.0) / ((__a * (1 - __ee)) / (magic * sqrtMagic) * __PI)
    dLng = (dLng * 180.0) / (__a / sqrtMagic * math.cos(radLat) * __PI)
    mgLat = lat + dLat
    mgLng = lng + dLng
    return lng * 2 - mgLng, lat * 2 - mgLat


def transformWGS84ToGCJ02(lng, lat):
    """
    WGS84 坐标 转 GCJ02
    :param lng:
    :param lat:
    :return:
    """
    if outOfChina(lng, lat):
        return [lng, lat]
    dLat = __transformLat(lng - 105.0, lat - 35.0)
    dLng = __transformLng(lng - 105.0, lat - 35.0)
    redLat = lat / 180.0 * __PI
    magic = math.sin(redLat)
    magic = 1 - __ee * magic * magic
    sqrtMagic = math.sqrt(magic)
    dLat = (dLat * 180.0) / ((__a * (1 - __ee)) / (magic * sqrtMagic) * __PI)
    dLng = (dLng * 180.0) / (__a / sqrtMagic * math.cos(redLat) * __PI)
    mgLat = lat + dLat
    mgLng = lng + dLng
    return mgLng, mgLat


def transformBD09ToWGS84(lng, lat):
    """
     百度坐标BD09 转 WGS84
    :param lng:
    :param lat:
    :return:
    """
    lngLat = transformBD09ToGCJ02(lng, lat)
    return transformGCJ02ToWGS84(lngLat[0], lngLat[1])


def transformWGS84ToBD09(lng, lat):
    """
    WGS84 转 百度坐标BD09
    :param lng:
    :param lat:
    :return:
    """
    lngLat = transformWGS84ToGCJ02(lng, lat)
    return transformGCJ02ToBD09(lngLat[0], lngLat[1])


def __transformLat(lng, lat):
    ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * math.sqrt(abs(lng))
    ret += (20.0 * math.sin(6.0 * lng * __PI) + 20.0 * math.sin(2.0 * lng * __PI)) * 2.0 / 3.0
    ret += (20.0 * math.sin(lat * __PI) + 40.0 * math.sin(lat / 3.0 * __PI)) * 2.0 / 3.0
    ret += (160.0 * math.sin(lat / 12.0 * __PI) + 320 * math.sin(lat * __PI / 30.0)) * 2.0 / 3.0
    return ret


def __transformLng(lng, lat):
    ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * math.sqrt(abs(lng))
    ret += (20.0 * math.sin(6.0 * lng * __PI) + 20.0 * math.sin(2.0 * lng * __PI)) * 2.0 / 3.0
    ret += (20.0 * math.sin(lng * __PI) + 40.0 * math.sin(lng / 3.0 * __PI)) * 2.0 / 3.0
    ret += (150.0 * math.sin(lng / 12.0 * __PI) + 300.0 * math.sin(lng / 30.0 * __PI)) * 2.0 / 3.0
    return ret


def outOfChina(lng, lat):
    """
    判断坐标是否不在国内
    :param lng:
    :param lat:
    :return:
    """
    return (lng < 72.004 or lng > 137.8347) or (lat < 0.8293 or lat > 55.8271)
